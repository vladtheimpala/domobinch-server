import express from 'express'
import { interval, Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject } from 'rxjs';
import { take, share, tap, shareReplay, distinct, distinctUntilChanged, switchMap, delay, filter, map} from 'rxjs/operators';
var morgan = require('morgan');             // log requests to the console (express4)

var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

var moment = require('moment');
moment().format();


const app = express();

app.use(express.static(__dirname + '/node_modules/heenok/dist'));                 // + '/public' set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
  console.error('***');
  // res.send('hello worolddd');
  // res.sendFile(__dirname + '/index.html');
  res.sendFile(__dirname + '/node_modules/heenok/dist/index.html');

});

// Replay last state pub/sub ---------------------------
const currentColorState$ = new ReplaySubject(1);
const currentColorStateObs$ = currentColorState$.asObservable()
  .pipe(
    shareReplay(1)
  );

const currentBrightnessState$ = new ReplaySubject(1);
const currentBrightnessStateObs$ = currentBrightnessState$.asObservable()
  .pipe(
    shareReplay(1)
  );



const currentWifiState$ = new ReplaySubject(1);
const currentWifiStateObs$ = currentWifiState$.asObservable()
  .pipe(
    shareReplay(1)
  );


const defaultAppFanEvent = {
  event: 'FAN',
  payload: {
    mode: null, // 1 min
  }
}


let fanDeviecEventInterval$; // device interval obs
let fanSocket;// socket of fan device
let fanSub;

const isDeviceEvent = { event: 'IDE', payload: { type: 'WHO' } };
const responseIsDeviceEvent = { event: 'IDE', payload: { type: 'WHO', value: 'DEVICE' } };
const readyEvent = { event: 'RED', payload: { value: true }};
const notReadyEvent = { event: 'RED', payload: { value: false }};
// const defaultFanEvent = { event: 'FAN', payload: { time: 1, interval:  }}; // 1 min

// { "event": "IDE","payload": { "type": "WHO","value": "DEVICE" } }

// { "event": "FAN","payload": { "mode": 2,"time":2, "interval": 4000 } }
// { "event": "FAN","payload": { "mode": 1,"time":1, "interval": 2000 } }


  // const timeout = () => setTimeout(() => {
  //   console.log('set to false');
  //   currentReadyState$.next(JSON.stringify(notReadyEvent));
  // }, 10000/*60 * 1000 * 60*/);

  // let bio = undefined;
  // let bsocket = undefined;

  const currentReadyState$ = new BehaviorSubject(notReadyEvent);

  // const broadcastReady$ = currentReadyState$
  //   .pipe(
  //     tap(event => bio.emit(event))
  //   )
    // .subscribe();

  // const emitReady$ = currentReadyState$
  //   .pipe(
  //     tap(event => bsocket.emit(event))
  //   )

  // const resetReady$ = currentReadyState$
  //     .pipe(
  //       map(event => JSON.parse(event)),
  //       switchMap(event => {
  //         if (event.payload.value) {
  //           return of()
  //             .pipe(
  //               delay(5000),
  //               tap(currentReadyState$.next(JSON.stringify(notReadyEvent)))
  //             )
  //         }
  //         else {
  //           return of()
  //         }
          
  //       })
        
  //     )
      // .subscribe();





    // .pipe(
    //   share(),
    //   tap(event => io.emit('message', event)),
      // switchMap((data) => {
        // console.log(data);
        // if (JSON.parse(data).payload.value) {
        //   return resetTimeout$;
        // } else {
        //   return of(data)
        // }
        // share again broadcast ? // no direct from forwarding
      // }),
      
   // )
  
    // const currentReadyStateSingle$ = currentReadyState$
    // .pipe(
    //   tap(event => socket.emit('message', event)),
    //   // switchMap((data) => {
    //     // console.log(data);
    //     // if (JSON.parse(data).payload.value) {
    //     //   return resetTimeout$;
    //     // } else {
    //     //   return of(data)
    //     // }
    //     // share again broadcast ? // no direct from forwarding
    //   // }),
      
    // )
    // currentReadyState$.subscribe();

    // const resetTimeout$ = currentReadyState$
    //   .pipe(
    //     map(data => JSON.parse(data)),
    //     filter(data => data.payload.value),
    //     delay(5000),
    //     tap(() => currentReadyState$.next(JSON.stringify(notReadyEvent)))
    //   ).subscribe();
// bug set false if don't clear timeout
// -----------------------------------------------------

const APP_FAN_MODE_1 = {
  event: 'FAN',
  payload: {
    mode: 1,
    time: 15,
    interval: 60 * 60 * 1000 / 2
  }
};
const APP_FAN_MODE_2 = {
  event: 'FAN',
  payload: {
    mode: 2,
    time: 15,
    interval: 60 * 60 * 1000
  }
};
const APP_FAN_MODE_3 = {
  event: 'FAN',
  payload: {
    mode: 3,
    time: 15,
    interval: 60 * 60 * 1000 * 2
  }
};

// const APP_FAN_MODE_OFF = {
//   event: 'FAN',
//   payload: {
//     mode: null
//   }
// };

const APP_FAN_MODE_1_KEY = 1;
const APP_FAN_MODE_2_KEY = 2;
const APP_FAN_MODE_3_KEY = 3;

const APP_FAN_MODE_EVENT_MAPPER = new Map()
  .set(APP_FAN_MODE_1_KEY, APP_FAN_MODE_1)
  .set(APP_FAN_MODE_2_KEY, APP_FAN_MODE_2)
  .set(APP_FAN_MODE_3_KEY, APP_FAN_MODE_3);
// let defaultFanModeEvent = 
const currentFanState$ = new ReplaySubject(1);
const currentFanStateObs$ = currentFanState$.asObservable()
  .pipe(
    shareReplay(1),
    filter(() => fanSocket)
  );

  let lastDisconnection;

console.log(JSON.stringify(isDeviceEvent), JSON.stringify(responseIsDeviceEvent));

const createFanModeInterval = (time, interv, socket) => {
  const event = {
    event: 'FANDEV',
    payload: {
      time
    }
  }
  return interval(interv)
  .pipe(
    tap(() => socket.emit(JSON.stringify(event))),
    tap(() => console.log('interval'))
  )
}

const init = () => {
  io.on('connection', socket => {
    console.log('client connected');
 

    // asking device ready    
    socket.emit('message', JSON.stringify(isDeviceEvent));


    // emit last state
    currentColorStateObs$
      .pipe(
        take(1),
        tap(msg => socket.emit('message', msg)),
      )
      .subscribe();

      currentBrightnessStateObs$
      .pipe(
        take(1),
        tap(msg => socket.emit('message', msg)),
      )
      .subscribe();

      currentFanStateObs$
      .pipe(
        take(1),
        filter(() => lastDisconnection && moment().add(3, 'hours').isAfter(lastDisconnection)),
        tap(msg => socket.emit('message', JSON.stringify(msg))),
      )
      .subscribe();

      currentWifiStateObs$
      .pipe(
        take(1),
        tap(msg => socket.emit('message', msg)),
      )
      .subscribe();


      currentReadyState$
      .pipe(
        take(1),
        tap(msg => socket.emit('message', JSON.stringify(msg)))
      )
      .subscribe();

    // listen for new message
    socket.on('message', msg => {



      console.log('msg receive', msg, typeof msg);



      if(msg && typeof msg === 'object') {
        console.log('1')
        //const msgObj = JSON.parse(msg);
        // READY RESP. LED+FAN

        if (
          msg && 
          msg.event === responseIsDeviceEvent.event &&
          msg.payload && 
          msg.payload.value === 'DEVICE'
        ) {
          console.log('device connected')
          // set interval to check it responds
          // tell clients it's ready 
          fanSocket = socket;
    
          currentReadyState$.next(readyEvent);
          
          if (fanSub) {
            fanSub.unsubscribe()
          }

          // fanDeviecEventInterval$ = 
            // fanSub = fanDeviecEventInterval$.subscribe()
            const now = moment();
            lastDisconnection

            if (lastDisconnection && moment().add(3, 'hours').isAfter(lastDisconnection)) {
              console.log('yes')
              fanSub = currentFanStateObs$.pipe(
                switchMap(event => createFanModeInterval(event.payload.time, event.payload.interval, fanSocket))
              ).subscribe()
            }
            





          io.emit('message', JSON.stringify(readyEvent))
          socket.on('disconnect', () => {
            console.log('device disconnected')
            currentReadyState$.next(notReadyEvent);
            io.emit('message', JSON.stringify(notReadyEvent))
            
            // if (fanSub) {
            //   fanSub.unsubscribe()
            // }
            lastDisconnection = moment();
            if (lastDisconnection && moment().add(3, 'hours').isAfter(lastDisconnection)) {
              if(fanSub){
                fanSub.unsubscribe();
              } 
            }
            fanSocket = null;
          });
          // check it's not going further
          return;
        }
        return;
      }


      console.log('3')
      // "save" last state
      if (msg.includes('WIF')) {
        currentWifiState$.next(msg);
      } else if (msg.includes('COL')) {
        currentColorState$.next(msg);
      } else if (msg.includes('BRI')) {
        currentBrightnessState$.next(msg);
      } else if (msg.includes('FAN')) {

        // if (fanSocket) {
          


// => parse mode
          if (fanSocket) {
            if (fanSub) {
              fanSub.unsubscribe()
            }
            const eventFanModeReceived = JSON.parse(msg);
            fanDeviecEventInterval$ = createFanModeInterval(eventFanModeReceived.payload.time, eventFanModeReceived.payload.interval, fanSocket);
            fanSub = fanDeviecEventInterval$.subscribe()
            currentFanState$.next(eventFanModeReceived);
         // }

          // AAA set mode to false
          }
          


          // const time = JSON.parse(msg).payload.time;
          // const interval = JSON.parse(msg).payload.interval;
     
      } 
      // broadcast message forward
      io.emit('message', msg);

    });

    socket.on('disconnect', () => {


    });




  });




}

init();

http.listen(port, () => {
  console.log('listening on *:' + port);
});
